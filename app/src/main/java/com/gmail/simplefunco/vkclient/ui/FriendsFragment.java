package com.gmail.simplefunco.vkclient.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.gmail.simplefunco.vkclient.R;
import com.gmail.simplefunco.vkclient.model.BaseResponsePojo;
import com.gmail.simplefunco.vkclient.model.FriendsResponsePojo;
import com.gmail.simplefunco.vkclient.model.UserPojo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vk.sdk.VKScope;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKUsersArray;

import java.util.ArrayList;
import java.util.List;

public class FriendsFragment extends Fragment {
    private static final String ACCESS_TOKEN_VK = "accessTokenVk";
    private static final String USER_ID_VK = "userIdVk";

    private String accessTokenVk;
    private String userIdVk;
    private ArrayAdapter<String> mFriendsNamesAdapter;
    private  List<UserPojo> mUserPojos = new ArrayList<>();
    private ArrayList<String> mUserNames = new ArrayList<>();
    private BaseResponsePojo mBaseResponsePojo;
    private FriendsResponsePojo mFriendsResponsePojo;

    public FriendsFragment() {
    }


    public static FriendsFragment newInstance(String accessToken, String userIdVk) {
        FriendsFragment fragment = new FriendsFragment();
        Bundle args = new Bundle();
        args.putString(ACCESS_TOKEN_VK, accessToken);
        args.putString(USER_ID_VK, userIdVk);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            accessTokenVk = getArguments().getString(ACCESS_TOKEN_VK);
            userIdVk = getArguments().getString(USER_ID_VK);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_friends, container, false);
        // VKRequest request = VKApi.users().get();
        VKRequest requestFriends = VKApi.friends().get(VKParameters.from(userIdVk));
        requestFriends.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);

                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();

                mFriendsResponsePojo = gson.fromJson(response.json.toString(), FriendsResponsePojo.class);
                mBaseResponsePojo = mFriendsResponsePojo.getBaseResponse();

                getUsersList(mBaseResponsePojo.getUserIds());
            }
            @Override
            public void onError(VKError error) {
                //Do error stuff
            }
            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                //I don't really believe in progress
            }
        });

        ListView lvFriends = (ListView) v.findViewById(R.id.lvFriends);

        mFriendsNamesAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, mUserNames);
        lvFriends.setAdapter(mFriendsNamesAdapter);

        return v;
    }

    private void getUsersList(List<Integer> usersIds) {
        VKRequest requestUsersList = VKApi.users().get(VKParameters.from("user_ids", usersIds));

        requestUsersList.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);

                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();

                mBaseResponsePojo = gson.fromJson(response.json.toString(), BaseResponsePojo.class);

                for (UserPojo user : mBaseResponsePojo.getResponse()) {
                    mUserNames.add(user.getFirstName());
                }
                mFriendsNamesAdapter.notifyDataSetChanged();
            }

            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                super.attemptFailed(request, attemptNumber, totalAttempts);
            }

            @Override
            public void onError(VKError error) {
                super.onError(error);
            }

            @Override
            public void onProgress(VKRequest.VKProgressType progressType, long bytesLoaded, long bytesTotal) {
                super.onProgress(progressType, bytesLoaded, bytesTotal);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
