package com.gmail.simplefunco.vkclient.model;
/* Created on 4/6/2017. */

import android.content.Intent;
import android.support.annotation.IntegerRes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class UserPojo {

    @SerializedName("id")
    @Expose
    @Getter @Setter
    private Integer id;

    @SerializedName("first_name")
    @Expose
    @Getter @Setter
    private String firstName;

    @SerializedName("last_name")
    @Expose
    @Getter @Setter
    private String lastName;
}
