package com.gmail.simplefunco.vkclient.controller;
/* Created on 4/6/2017. */

import com.vk.sdk.api.VKRequest;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class CallbackHellResolver {
    @Getter @Setter
    private List<VKRequest> mVKRequests;

    /**
     * <p>Starts next request if exists. In case of last
     * request - returns false.</p>
     *
     *  <p>Returns true if request started; false if nothing
     * to start (List is empty)</p>
     */
    public boolean startNextRequest() {

        return false;
    }
}
