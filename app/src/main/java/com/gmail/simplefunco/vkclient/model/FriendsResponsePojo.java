package com.gmail.simplefunco.vkclient.model;
/* Created on 4/6/2017. */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class FriendsResponsePojo {

    @SerializedName("response")
    @Expose
    @Getter @Setter
    private BaseResponsePojo baseResponse;
}
