package com.gmail.simplefunco.vkclient.model;
/* Created on 4/6/2017. */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class BaseResponsePojo {

    @SerializedName("response")
    @Expose
    @Getter @Setter
    private List<UserPojo> response = null;

    @SerializedName("items")
    @Expose
    @Getter @Setter
    private List<Integer> userIds = null;

    @SerializedName("count")
    @Expose
    private Integer count = null;
}
